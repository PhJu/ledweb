import os
import signal
import subprocess
import time
import logging
from logging import Formatter, FileHandler
import json
import psutil

from flask import render_template, request, redirect, url_for, flash
from flask_security import Security, SQLAlchemyUserDatastore, login_required

from app import app, db
from app.models import Effect, RunningEffect, Setting, User, Role
from app.forms import EffectForm, SettingForm

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)



@app.route("/")
def home():
    """ Main Page listing all effects available
    """
    effects = Effect.query.all()
    return render_template("pages/home.html", effects=effects)


@app.route("/effect/<effect_id>/")
def effect_control(effect_id):
    """ Show the effect and it's settings
    """
    effect = Effect.query.filter_by(id=effect_id).first()
    return render_template("pages/effect_control.html", effect=effect)
    
    
@app.route("/effect/running/")
def effect_running():
    """ API endpoint returning the running effect id. 
        If the effect somehow died afer being started, the database entry will be updated.
    """
    running_return = {"running": []}
    running_effects = RunningEffect.query.all()
    for running in running_effects:
        try:
            # Check if child process exists
            os.waitpid(running.pid, os.WNOHANG)
        except ChildProcessError:
            pass

        # Check if the PID actually exists. If not the process probably did'nt start properly
        if psutil.pid_exists(running.pid):
            running_return["running"].append(running.effect_id)
        else:
            # Delete the wrong database record
            db.session.delete(running)
            db.session.commit()
    return running_return


@app.route("/effect/<effect_id>/play/", methods=["POST"])
def effect_play(effect_id):
    """ API endpoint to start or stop a effect. If a effect is already running
        this will stop the running effect.
        If the running effect and the to-be-played effect differ, two SIGINT are sent to the child process.
        This should skip the shutdown of the LEDs.
        If the to-be-played effect is already running, it will be just stopped and not started.
    """
    # Get the to-be-played and already running Effect database records
    effect = Effect.query.filter_by(id=effect_id).first()
    running_effect = None

    if effect.independent:
        # If current Effect is indpendent, look if it's already running
        running_effect = RunningEffect.query.filter_by(effect_id=effect.id).first()
    else:
        # If Effect ist not independent, look for other running effects that are not independent
        running_effects = RunningEffect.query.all()
        for running_effect_loop in running_effects:
            if not Effect.query.filter_by(id=running_effect_loop.effect_id).first().independent:
                running_effect = running_effect_loop
                break
        
    running_effect_id = 0
    
    # Do not mess with the process, if it's already shutting down.
    if running_effect and not running_effect.stopping:
        running_effect_id = running_effect.effect_id
        running_effect.stopping = True
        db.session.add(running_effect)
        db.session.commit()
        # Save that the running_effect is going to be stopped.
        try:
            # Send SIGINT (eq. Ctrl+C) to the running process
            os.kill(running_effect.pid, signal.SIGINT)
            if running_effect_id != effect.id or request.form.get("action") == "restart":
                # Send it twice if a different effect is to-be-played
                time.sleep(0.01)
                os.kill(running_effect.pid, signal.SIGINT)
            while True:
                # Wait for the process to end. Throws a ChildProcessError if it had ended
                os.waitpid(running_effect.pid, os.WNOHANG)
                time.sleep(0.05)
        except (OSError, ChildProcessError):
            pass
        # Delete running_effect from db
        db.session.delete(running_effect)
        db.session.commit()

    # If a different effect is to-be-played or "restart" is issued
    if running_effect_id != effect.id or request.form.get("action") == "restart":
        # Prepare subprocess call with configured command and settings
        args = effect.command.split()
        for setting in effect.settings:
            args.append(setting.key)
            args.append(setting.value)
        running_effect = RunningEffect()
        # Start the childprocess and save it's PID in the db
        running_effect.pid = subprocess.Popen(args, env=json.loads(effect.env)).pid
        running_effect.effect_id = effect.id
        db.session.add(running_effect)
        db.session.commit()
        return {"running":True}
    return {"running":False}


@app.route("/effect/<effect_id>/save_settings/", methods=["POST"])
def effect_save(effect_id):
    """ API Endpoint to save each setting of a effect.
    """
    effect = Effect.query.filter_by(id=effect_id).first()
    for setting in effect.settings:
        setting.value = request.form[str(setting.id)]
    db.session.add(effect)
    db.session.commit()
    return {"status": "OK", "text": "Gespeichert", "iconHtml": "<span class=\"oi oi-check\"></span>"}


@app.route("/effect/add/", methods=["GET", "POST"])
@login_required
def add_effect():
    """ Form to create a new effect
    """
    form = EffectForm()
    if form.validate_on_submit():
        effect = Effect()
        form.populate_obj(effect)
        
        db.session.add(effect)
        db.session.commit()
        flash("Effekt erfolgreich angelegt", "success")
        return redirect(url_for("effect_control", effect_id=effect.id))
    return render_template("pages/effect_add.html", form=form)


@app.route("/effect/<effect_id>/edit/", methods=["GET", "POST"])
@login_required
def edit_effect(effect_id):
    """ Form to edit a existing effect
    """
    effect = Effect.query.filter_by(id=effect_id).first()
    form = EffectForm()
    if form.validate_on_submit():
        form.populate_obj(effect)

        db.session.add(effect)
        db.session.commit()
        flash("Effekt erfolgreich geändert", "success")
        return redirect(url_for("effect_control", effect_id=effect.id))
    form.process(obj=effect)
    return render_template("pages/effect_edit.html", form=form, effect_id=effect.id)


@app.route("/effect/<effect_id>/delete/", methods=["GET", "POST"])
@login_required
def delete_effect(effect_id):
    """ Form to delete a existing effect. Will delete it's settings too
    """
    effect = Effect.query.filter_by(id=effect_id).first()
    for setting in effect.settings:
        db.session.delete(setting)
    db.session.delete(effect)
    db.session.commit()

    flash("Effekt erfolgreich gelöscht", "success")
    return redirect(url_for("home"))


@app.route("/effect/<effect_id>/add_setting/", methods=["GET", "POST"])
@login_required
def add_setting(effect_id):
    """ Form to add new settings to a effect
    """
    form = SettingForm()
    if form.validate_on_submit():
        effect = Effect.query.filter_by(id=effect_id).first()
        setting = Setting()
        form.populate_obj(setting)
        setting.effect = effect
        db.session.add(setting)
        db.session.commit()
        
        flash("Einstellung erfolgreich angelegt", "success")
        return redirect(url_for("effect_control", effect_id=effect.id))
    return render_template("pages/setting_add.html", form=form)


@app.route("/setting/<setting_id>/edit/", methods=["GET", "POST"])
@login_required
def edit_setting(setting_id):
    """ Form to edit a existing setting
    """ 
    setting = Setting.query.filter_by(id=setting_id).first()
    form = SettingForm()
    if form.validate_on_submit():
        form.populate_obj(setting)
        db.session.add(setting)
        db.session.commit()
        
        flash("Einstellung erfolgreich geändert", "success")
        return redirect(url_for("effect_control", effect_id=setting.effect.id))
    form.process(obj=setting)
    return render_template("pages/setting_edit.html", form=form, setting_id=setting.id)


@app.route("/setting/<setting_id>/delete/")
@login_required
def delete_setting(setting_id):
    """ Form to delete a existing setting
    """
    setting = Setting.query.filter_by(id=setting_id).first()
    effect_id = setting.effect.id
    db.session.delete(setting)
    db.session.commit()
        
    flash("Einstellung erfolgreich gelöscht", "success")
    return redirect(url_for("effect_control", effect_id=effect_id))
    
# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template("errors/500.html"), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template("errors/404.html"), 404


if not app.debug:
    file_handler = FileHandler("error.log")
    file_handler.setFormatter(
        Formatter("%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]")
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info("errors")

# ----------------------------------------------------------------------------#
# Launch.
# ----------------------------------------------------------------------------#


if __name__ == "__main__":
    app.run("0.0.0.0")
