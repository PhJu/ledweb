# LedWeb
*Working title - since LANLight is already copyrighted*

A customizable and mobile-friendly webapp to control your LED lighting software.

Powered by the [Flask micro framework](https://github.com/pallets/flask/).

## Disclaimer
This software is intended for a local network environment only. Use at your own risk.

It currently allows any logged in user to execute any program with the webserver's user permissions.

## Basic installation
While I recommend configuring UWSGI to make everything work with your webserver, this basic installation should work just fine.

After cloning this git, set up a virtual environment and install the requirements listed in `requirements.txt`.
```bash
python -m pip install virtualenv
cd ledweb
virtualenv ledweb_venv
source ledweb_venv/bin/activate
pip install -r requirements.txt
```

Initialise the Database. This command can be run to update an existing installation.
````bash
flask db upgrade
````
Create an administrative User using the interactive Python Interpreter.
```python
>>> from app import db, models
>>> u = models.User(email="your login name here", password="secretpassword", active=True)
>>> db.session.add(u)
>>> db.session.commit()
>>> exit()
```
To make it visible to other devices in your Network edit the last few lines in `ledweb.py`.
````python
if __name__ == "__main__":
    app.run('0.0.0.0')
````
Finally start the builtin development server.
```bash
python ledweb.py
```

Finally navigate to your device address `http://example.local:5000/` on your Webbrowser.

## Getting started
WIP

### Adding Effects

### Adding and managing Settings

## ToDos:

 - Translations
 - Documentation
 - CI?
