"""Remove independent fk

Revision ID: 407c7e2940f5
Revises: 8cc4af8ffdec
Create Date: 2020-03-09 15:21:19.806679

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '407c7e2940f5'
down_revision = '8cc4af8ffdec'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('RunningEffect') as batch_op:
        batch_op.drop_column('independent')


def downgrade():
    with op.batch_alter_table('RunningEffect') as batch_op:
        batch_op.add_column( sa.Column('independent', sa.Boolean(), nullable=True))
