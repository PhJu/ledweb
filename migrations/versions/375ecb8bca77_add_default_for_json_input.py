"""Add default for JSON input

Revision ID: 375ecb8bca77
Revises: 407c7e2940f5
Create Date: 2020-03-09 14:23:01.783944

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import sqlite

# revision identifiers, used by Alembic.
revision = '375ecb8bca77'
down_revision = '407c7e2940f5'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('Effect') as batch_op:
        batch_op.drop_column('env')
        
        batch_op.add_column(sa.Column('env', sa.JSON(), nullable=False))


def downgrade():
    with op.batch_alter_table('Effect') as batch_op:
        batch_op.drop_column('env')
        
        batch_op.add_column(sa.Column('env', sa.JSON(), nullable=True))
