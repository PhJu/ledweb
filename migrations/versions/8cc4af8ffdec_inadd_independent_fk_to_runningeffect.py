"""InAdd independent FK to RunningEffect

Revision ID: 8cc4af8ffdec
Revises: 53cf105cda26
Create Date: 2020-03-02 23:21:08.658787

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8cc4af8ffdec'
down_revision = '53cf105cda26'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('RunningEffect') as batch_op:
        batch_op.add_column( sa.Column('independent', sa.Boolean(), nullable=True))


def downgrade():
    with op.batch_alter_table('RunningEffect') as batch_op:
        batch_op.drop_column('independent')
