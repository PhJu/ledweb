"""Independent Effects

Revision ID: 53cf105cda26
Revises: 337a2cf9c00b
Create Date: 2020-03-02 22:49:09.438174

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '53cf105cda26'
down_revision = '337a2cf9c00b'
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table('Effect') as batch_op:
        batch_op.add_column(sa.Column('independent', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    with op.batch_alter_table('Effect') as batch_op:
        batch_op.drop_column('independent')