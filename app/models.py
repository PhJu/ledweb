from app import db
from flask_security import UserMixin, RoleMixin
from sqlalchemy import JSON


class Effect(db.Model):
    __tablename__ = "Effect"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    description = db.Column(db.String(128))
    command = db.Column(db.String(255), nullable=False)
    env = db.Column(JSON, nullable=False, default="{}")
    independent = db.Column(db.Boolean(), default=False)


class RunningEffect(db.Model):
    __tablename__ = "RunningEffect"

    id = db.Column(db.Integer, primary_key=True)
    pid = db.Column(db.Integer, nullable=False)
    stopping = db.Column(db.Boolean(), default=False, nullable=False)
    effect_id = db.Column(db.Integer, db.ForeignKey("Effect.id"), nullable=False)


class Setting(db.Model):
    __tablename__ = "Setting"

    id = db.Column(db.Integer, primary_key=True)
    control_type = db.Column(db.Integer, nullable=False, default=0)
    label = db.Column(db.String(40), nullable=False)
    key = db.Column(db.String(20), nullable=False)
    value = db.Column(db.String(20))
    effect_id = db.Column(db.Integer, db.ForeignKey("Effect.id"), nullable=False)
    effect = db.relationship("Effect", backref=db.backref("settings", lazy=True))

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    active = db.Column(db.Boolean(), nullable=False, default=False)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
