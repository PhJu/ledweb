function outputUpdate(value, id) {
    /* Update the setting output to show the selected value
     */
    id = "#" + id + "_out";
    document.querySelector(id).value = value;
}

function submitForm(url, id) {
    /* Submit a form using an ajax request
     */
    id = "#" + id;
    var text = document.querySelector(id).textContent;
    document.querySelector(id).disabled = true;
    $.ajax({
        type: 'POST',
        url: url,
        data: $('form').serialize(),
        success: function (response) {
            document.querySelector(id).innerHTML = response.text + " " + response.iconHtml;
            setTimeout(function () {
                document.querySelector(id).textContent = text;
                document.querySelector(id).disabled = false;
            }, 2000);
        },
        error: function (error) {
            console.log(error);
        }
    });
}

$(document).ready(function () {
    /* Automatically dismiss a alert popup after one second
     */
    $(".alert-success").fadeTo(1000, 500).slideUp(500, function () {
        $(".alert-success").slideUp(500);
    });
});

$(function () {
  $('.pop_tooltip').tooltip()
});
