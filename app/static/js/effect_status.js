var status_btns = $(".effect_status");

// HTML code of various icons
//const play_icon = "<span class=\"oi oi-media-play\"></span>";
const play_icon = "<i data-feather=\"play\"></i>";
//const stop_icon = "<span class=\"oi oi-media-stop\"></span>";
const stop_icon = "<i data-feather=\"square\"></i>";
//const warning_icon = "<span class=\"oi oi-warning\"></span>";
const warning_icon = "<i data-feather=\"alert-triangle\"></i>";

$(document).ready(function () {
    // keep refreshing the status every 5 seconds
    var getStatusLoop = function () {
        getStatus();

        setTimeout(function () {
            getStatusLoop();
        }, 5000);
    };

    getStatusLoop();
});

var getStatus = function () {
    /* Request from the status endpoint which effect is running if any.
       Updates the "Play" or "Stop" button accordingly.
    */
    $.ajax({
        type: 'GET',
        url: $("#effect_running").data("url"),
        timeout: 5000,
        success: function (response) {
            status_btns.each(function (i, obj) {
                var status_btn = $(obj);

                status_btn.removeClass("btn-outline-warning");
                status_btn.prop("disabled", false);

                // update the button status
                if (response.running.indexOf(status_btn.data("effectid")) > -1) {
                    if (status_btn.hasClass("effect_status_icon")) {
                        status_btn.html(stop_icon);
                    }
                    else {
                        status_btn.text("Stop");
                    }
                    status_btn.addClass("btn-danger");
                    status_btn.removeClass("btn-success");
                }
                else {
                    if (status_btn.hasClass("effect_status_icon")) {
                        status_btn.html(play_icon);
                    }
                    else {
                        status_btn.text("Play");
                    }
                    status_btn.addClass("btn-success");
                    status_btn.removeClass("btn-danger");
                }
            });
            var reload_btn = $("#effect_restart");
            if (reload_btn && response.running.indexOf(reload_btn.data("effectid")) > -1) {
                reload_btn.removeClass("btn-outline-warning");
                reload_btn.addClass("btn-warning");
                reload_btn.prop("disabled", false);
            }
            else if (reload_btn) {
                reload_btn.removeClass("btn-warning");
                reload_btn.addClass("btn-outline-warning");
                reload_btn.prop("disabled", true);
            };
            
            feather.replace({'stroke-width': 2 });
        },
        error: function (error) {
            console.log(error);
        }
    });

};

function submitEffectPlay(url, id, restart=false) {
    /* Submit the play button press to it's API endpoint
    */
    if (restart) {
        var params = {action: "restart"};
    }
    else {
        var params = null;
    }
    
    var submit_btn = $("#" + id);
    submit_btn.prop("disabled", true);
    $.ajax({
        type: 'POST',
        url: url,
        data: params,
        success: function (response) {
            getStatus();
        },
        error: function (error) {
            console.log(error);
        }
    });
}