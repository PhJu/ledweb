import os

class Config(object):
    # Grabs the folder where the script runs.
    basedir = os.path.abspath(os.path.join(os.path.dirname(__file__)))

    # Enable debug mode.
    DEBUG = False

    # Secret key for session management. You can generate random strings here:
    # https://randomkeygen.com/
    SECRET_KEY = '=s9bBRj~KmfQ)9uFJb:u,9/jW_~wk^7Og#G@27Wl82W5:ft>Knke[3"O<__I_6M'

    # Connect to the database
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Add some spice to your encrypted passwords which not approved by the german government...
    SECURITY_PASSWORD_SALT = 'h/J!pv?0Tvno0C(gQji+Vb{G]^mYr]/\'7,5\'MDQwD5dML<gy</-t~#Le#|K&eIv'