from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, TextAreaField, BooleanField
from wtforms.validators import DataRequired, Length


class EffectForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired(), Length(max=40)])
    description = StringField('Beschreibung', validators=[Length(max=128)])
    command = StringField('Befehl', validators=[DataRequired(),Length(max=255)])
    env = TextAreaField('Umgebungsvariablen (JSON)', validators=[DataRequired(),Length(max=255)], default="{}")
    independent = BooleanField('Unabhängig')
    
class SettingForm(FlaskForm):
    label = StringField('Bezeichner', validators=[DataRequired(),Length(max=40)])
    control_type = SelectField('Steuerungstyp', choices=[
        ("1", "Range Input"), 
        ("2", "Color Picker")
    ], validators=[DataRequired()])
    key = StringField('Parameter', validators=[DataRequired(),Length(max=20)])